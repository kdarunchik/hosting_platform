<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 
        'provider_id', 
        'name',
        'access_token',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function provider()
    {
        return $this->belongsTo('App\Models\Provider');
    }
}
