<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'url',
        'logo',
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\User','tokens', 'provider_id', 'user_id')->withPivot('name', 'access_token')->withTimestamps();
    }

    public function servers()
    {
        return $this->hasMany('App\Models\Server');
    }
}
