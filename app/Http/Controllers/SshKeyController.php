<?php

namespace App\Http\Controllers;

use App\Models\SshKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SshKeyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.sshkeys');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $key = SshKey::updateOrCreate(
            ['user_id' => Auth::id(), 'name' => $request->name],
            ['public_key' => $request->public_key]
        );
        return response()->json(['key' => $key]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SshKey  $sshKey
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $keys = Auth::user()->ssh_keys()->select('id', 'name')->orderBy('name', 'asc')->get()->toArray();
        return response()->json(['keys' => $keys]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SshKey  $sshKey
     * @return \Illuminate\Http\Response
     */
    public function edit(SshKey $sshKey)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SshKey  $sshKey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SshKey $sshKey)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SshKey  $sshKey
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $key = SshKey::find($id);
        if(empty($key)){
            return redirect()->route('ssh_keys');
        }
        if ($key->user_id == Auth::id()) {
            $key->delete();
            return response()->json(['success' => $id]);
        } else {
            return redirect()->route('ssh_keys');
        }
        return true;
    }
}
