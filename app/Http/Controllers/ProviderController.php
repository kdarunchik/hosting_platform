<?php

namespace App\Http\Controllers;

use App\Models\Provider;
use App\Models\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProviderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.providers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show(Provider $provider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function edit(Provider $provider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provider $provider)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provider $provider)
    {
        //
    }

    public function list()
    {
        $providers = Provider::orderBy('name', 'asc')->get()->toArray();
        return response()->json(['providers' => $providers]);
    }

    public function getToken($providerId)
    {
        $token = Token::where("user_id", Auth::id())->where("provider_id", $providerId)->first();
        if($token) {
            $token->toArray;
        }
        return response()->json(['token' => $token]);
    }

    public function setToken(Request $request)
    {
        $providerId = $request->providerId;
        $dataToken = $request->input;
        if($token = Token::where('user_id', Auth::id())->where('provider_id', $providerId)->first()) {
            $token->name = $dataToken['name'];
            $token->access_token = $dataToken['access_token'];
            $token->save();
        } else {
            $token = Token::create(
                [
                    'user_id' => Auth::id(), 
                    'provider_id' => $providerId,
                    'name' => $dataToken['name'],
                    'access_token' => $dataToken['access_token']
                ]
            )->save();
        }
        
        return response()->json(['token' => $token]);
    }

    public function deleteToken($providerId)
    {
        $token = Token::where('user_id', Auth::id())->where('provider_id', $providerId)->first();
        if(empty($token)){
            return redirect()->route('dns_providers');
        } else {
            $token->delete();
            return response()->json(['token' => '']);
        }
        return true;
    }
}
