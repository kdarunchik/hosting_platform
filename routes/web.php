<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::prefix('settings')->group(
    function () {
        Route::get('profile', 'HomeController@profile')->name('profile');
        Route::get('ssh_keys', 'SshKeyController@index')->name('ssh_keys');
        Route::get('providers', 'ProviderController@index')->name('dns_providers');
    }
);

Route::prefix('api')->group(
    function () {
        Route::prefix('settings')->group(
            function () {
                Route::post('profile', 'HomeController@updateProfile');
                Route::post('key', 'SshKeyController@store');
                Route::post('key/delete/{id}', 'SshKeyController@destroy');
            }
        );
        Route::get('ssh_keys', 'SshKeyController@list');
        Route::get('providers', 'ProviderController@list');
        Route::get('provider/token/{providerId}', 'ProviderController@getToken');
        Route::post('provider/token', 'ProviderController@setToken');
        Route::post('provider/token/delete/{providerId}', 'ProviderController@deleteToken');
    }
);