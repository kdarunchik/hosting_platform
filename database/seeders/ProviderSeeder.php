<?php

namespace Database\Seeders;

use App\Models\Provider;
use Illuminate\Database\Seeder;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return Provider::create(
            [
                'name' => 'Digital Ocean',
                'url' => 'https://api.digitalocean.com/v2/',
                'logo' => 'https://images.prismic.io/www-static/49aa0a09-06d2-4bba-ad20-4bcbe56ac507_logo.png?auto=compress,format',
            ]
        );
    }
}
