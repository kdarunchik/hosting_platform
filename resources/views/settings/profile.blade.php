@extends('layouts.app')

@section('content')
<profile-page :user="{{ auth()->check() ? auth()->user() : 'false' }}"></profile-page>
@endsection
