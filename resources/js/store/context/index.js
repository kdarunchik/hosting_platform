export default {
    namespaced: true,
    state: {
        accessible_companies: [],
        current_company_id: null,
        accessible_sites: [],
        current_site_id: null
    },
    mutations: {
        SET_CONTEXT(state, context) {
            state.accessible_companies = context.accessible_companies;
            state.current_company_id = context.current_company_id;
            state.accessible_sites = context.accessible_sites;
            state.current_site_id = context.current_site_id;
        },
        CHANGE_COMPANY(state, companyId) {
            state.current_company_id = companyId;
        },
        UPDATE_SITES(state, sites) {
            state.accessible_sites = [...sites];
            state.current_site_id = 0;
        },
        CHANGE_SITE(state, siteId) {
            state.current_site_id = siteId;
        }
    },
    actions: {
        setContext({ commit }) {
            return axios
                .get("/api/context")
                .then(response => {
                    commit("SET_CONTEXT", response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        changeCompany({ commit }, formData) {
            return axios
                .put("/api/context/changeCompany", formData)
                .then(response => {
                    commit("CHANGE_COMPANY", response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        },
        updateSites({ commit }) {
            return axios
                .get("/api/context/getSites")
                .then(response => {
                    commit("UPDATE_SITES", response.data);
                })
                .catch(error => console.log(error));
        },
        changeSite({ commit }, formData) {
            return axios
                .put("/api/context/changeSite", formData)
                .then(response => {
                    commit("CHANGE_SITE", response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }
};