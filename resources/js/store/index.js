import Vuex from "vuex";
import Vue from "vue";

import context from "./context";

Vue.use(Vuex);

const store = new Vuex.Store({
    context
});

export default store;