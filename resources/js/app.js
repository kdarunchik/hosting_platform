/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import "babel-polyfill";

require('./bootstrap');
window.Vue = require("vue").default;

import "@mdi/font/css/materialdesignicons.css";
import Vuetify from "vuetify";

Vue.use(Vuetify);

const vuetify = new Vuetify({
    icons: {
        iconfont: "mdi" // default - only for display purposes
    }
});

import Vuex from "vuex";
Vue.use(Vuex);
import store from "./store";


Vue.component("app-layout", require("./layouts/AppLayout.vue").default);
Vue.component("dashboard-page", require("./pages/Dashboard.vue").default);
Vue.component("profile-page", require("./pages/Profile.vue").default);
Vue.component("ssh-keys-page", require("./pages/Sshkeys.vue").default);
Vue.component("providers-page", require("./pages/Providers.vue").default);


const app = new Vue({
    el: "#app",
    store,
    vuetify
});